//
//  InfoConteudoController.swift
//  primeirosSocorros
//
//  Created by Thiago Bessa on 13/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

//////////////////////////
class CellItens: UITableViewCell{

 
    @IBOutlet var conteudoCelula: UILabel!

    @IBOutlet var ImageCelula: UIImageView!

}



struct estruturaDosDados{
    
    var dadosDoPassoAPasso = String()
    var imagemDoPassoAPasso = String()
    
    init(dadosDoPassoAPasso: String, imagemDoPassoAPasso: String) {
        
        self.dadosDoPassoAPasso = dadosDoPassoAPasso
        self.imagemDoPassoAPasso = imagemDoPassoAPasso
        
    }
}

////////////////////////
var rcp_passos = [estruturaDosDados]()
var vias_passos = [estruturaDosDados]()
var afo_passos = [estruturaDosDados]()
var sangra_passos = [estruturaDosDados]()
var queimadu_passos = [estruturaDosDados]()
var fratura_passos = [estruturaDosDados]()
var avc_passos = [estruturaDosDados]()
var convulsao_passos = [estruturaDosDados]()
var intox_passos = [estruturaDosDados]()
var passo_atual = [estruturaDosDados]()
var rcp = false//
var vias = false//
var afogamento = false//
var sangramento = false//
var queimadura = false//
var fratura = false//
var avc = false
var convulsao = false
var intox = false
var kitt = false


class InfoConteudoController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var myView = UIScrollView(frame: CGRect(x: 0, y: 60, width: 600, height: 700))

   
  
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var videoPreviewLayer: UIView!
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    func playVi(nome: String){
        let moviePath = Bundle.main.path(forResource:nome , ofType: "mp4")
        if let path = moviePath{
            let url = NSURL.fileURL(withPath: path)
            let item = AVPlayerItem(url: url)
            self.player = AVPlayer(playerItem: item)
            self.avpController = AVPlayerViewController()
            self.avpController.player = self.player
            avpController.view.frame = videoPreviewLayer.frame
            self.addChildViewController(avpController)
            self.view.addSubview(avpController.view)
            print("func para abrir")
            print(player.status)
        
        }

    }
    @IBOutlet weak var seg: UISegmentedControl!
    
    var informacao: Int!
    
    
    
    var conteudo = [["RCP","Conteudo"],["Obstrução","Conteudo"],["Afogamento","Conteudo"],["Sangramentos","Conteudo"],["Queimadura","Conteudo"],["Fratura","Conteudo"],["Convulsão","Conteudo"],["Intoxicação","Conteudo"],["AVC","Conteudo"],["KIT","Conteudo"]]
    
    var ConteudoInf: [String]!
    
//    @IBOutlet weak var navigationTitulo: UINavigationItem!

    func scrool_introducao(){
        myView.contentSize = CGSize(width: 0, height: 1775)
        var imagem2 : UIImageView = UIImageView()
        imagem2.frame = CGRect(x: 0, y: 20, width: 375, height: 1525)
        imagem2.image = UIImage(named: "fundoIntro")
        
        
       
        self.view.addSubview(myView)
       self.myView.addSubview(imagem2)
      

    }
    
    @IBAction func seg_ac(_ sender: Any) {
        if player != nil{
            player.pause()
        }
        
        self.tableview.setContentOffset(CGPoint.zero, animated: false)
        
        if rcp==true{
        if seg.selectedSegmentIndex == 0{
            
          scrool_introducao()
           
            
     
           
        }
        if seg.selectedSegmentIndex == 1{


       
            rcp_passos = [estruturaDosDados(dadosDoPassoAPasso: "Se o neonato não estiver respirando ou respondendo a estímulos externos, ligue 192 (Samu) ou peça para alguém ligar para você;", imagemDoPassoAPasso: "1" ),
                          estruturaDosDados(dadosDoPassoAPasso: "Solicite um DEA (Desfribilador Externo Automático) com pás pediátricas;", imagemDoPassoAPasso: "2" ),estruturaDosDados(dadosDoPassoAPasso: "Enquanto o DEA não chega, inicie o RCP imediatamente;", imagemDoPassoAPasso: "3" ),estruturaDosDados(dadosDoPassoAPasso: "Com o neonato em uma superfície rígida, posicione dois dedos no meio do peito e realize as compressões. Lembre-se que o berço não é considerado superfície rígida; ", imagemDoPassoAPasso: "4" ),estruturaDosDados(dadosDoPassoAPasso: "Até um mês de vida, faça 1 respiração boca a boca e três compressões. O ideal é que os pais do bebê façam a respiração para não haver troca de fluidos com estranhos;", imagemDoPassoAPasso: "5" ),estruturaDosDados(dadosDoPassoAPasso: "Lembre de sempre deixar o tórax do neonato retornar por completo;", imagemDoPassoAPasso: "6" ),estruturaDosDados(dadosDoPassoAPasso: "Após a chegada do DEA, posicione o aparelho ao lado do bebê e ligue-o;", imagemDoPassoAPasso: "7" ),estruturaDosDados(dadosDoPassoAPasso: "Posicione uma pá no meio do peito do bebê e outra no mesmo lugar, mas nas costas;", imagemDoPassoAPasso: "8" ),estruturaDosDados(dadosDoPassoAPasso: "Afaste-se do neonato para que o DEA faça a análise do ritmo cardíaco corretamente;", imagemDoPassoAPasso: "9" ),estruturaDosDados(dadosDoPassoAPasso: "Após a análise, o DEA irá decidir se o ritmo da vítima é chocável ou não. Se for, ele irá carregar o choque, senão, ele solicitará que você continue as compressões;", imagemDoPassoAPasso: "10" ),estruturaDosDados(dadosDoPassoAPasso: "Não esqueça de fazer as compressões enquanto o DEA carrega o choque. Pare somente quando ele disser “choque carregado”;", imagemDoPassoAPasso: "11" ),estruturaDosDados(dadosDoPassoAPasso: "Peça para que todos se afastem e aperte o botão de choque, quando necessário;", imagemDoPassoAPasso: "12" ),estruturaDosDados(dadosDoPassoAPasso: "Reinicie o RCP logo após o choque. A cada 2 minutos o DEA irá analisar o ritmo do bebê para atualizar seus sinais vitais;", imagemDoPassoAPasso: "13" ),estruturaDosDados(dadosDoPassoAPasso: "Pare as compressões somente quando a equipe de socorro chegar ao local e assumir a situação, quando o neonato reagir ou quando o socorrista estiver exausto.", imagemDoPassoAPasso: "14" )]
           
playVi(nome: "rcp_neo")
         
            passo_atual = rcp_passos
            self.tableview.reloadData()
            myView.removeFromSuperview()
          
         
      
           
            
            
   
        }
        if seg.selectedSegmentIndex == 2{
            rcp_passos = [estruturaDosDados(dadosDoPassoAPasso: "Verifique se a vítima está consciente: bata sua mão na superfície ao lado, perto dos ouvidos da vítima, e grite “senhor?” ou “senhora?” para verificar se a mesma responde;", imagemDoPassoAPasso: "1" ),
                          estruturaDosDados(dadosDoPassoAPasso: "Verifique se a vítima está respirando: posicione sua cabeça de uma forma que dê para perceber a movimentação da caixa torácica da mesma; ", imagemDoPassoAPasso: "2" ),estruturaDosDados(dadosDoPassoAPasso: "Se a vitima respirar, mas não responder, coloque-a em posição de drenagem e monitore seus sinais vitais; ", imagemDoPassoAPasso: "3" ),estruturaDosDados(dadosDoPassoAPasso: "Se a vítima não respirar, chame por ajuda. Ligue para o 192 (Samu) ou peça para alguém fazer isso por você; ", imagemDoPassoAPasso: "4" ),estruturaDosDados(dadosDoPassoAPasso: "DEA: Solicite o Desfribilador Externo Automático;", imagemDoPassoAPasso: "5" ),estruturaDosDados(dadosDoPassoAPasso: "Enquanto o DEA não chega, inicie as compressões torácicas imediatamente;  ", imagemDoPassoAPasso: "6" ),estruturaDosDados(dadosDoPassoAPasso: "Entrelace as mãos uma na outra deixando o calcanhar da mão evidente (instruções no vídeo); ", imagemDoPassoAPasso: "7" ),estruturaDosDados(dadosDoPassoAPasso: "Posicione suas mãos no meio do peito da vítima e faça de 100 a 120 compressões por minuto, com uma profundidade entre 5cm a 6cm. Sempre deixe o tórax da vítima retornar por completo; ", imagemDoPassoAPasso: "8" ),estruturaDosDados(dadosDoPassoAPasso: "Minimize interrupções e troque de socorrista, se possível, a cada 2 minutos enquanto o DEA não chega;", imagemDoPassoAPasso: "9" ),estruturaDosDados(dadosDoPassoAPasso: "Após a chegada do DEA, posicione o aparelho ao lado da vítima e ligue-o;", imagemDoPassoAPasso: "10" ),estruturaDosDados(dadosDoPassoAPasso: "Posicione uma pá abaixo da clavícula direita e outra embaixo da mama esquerda, perto das costelas, e as conecte no DEA;", imagemDoPassoAPasso: "11" ),estruturaDosDados(dadosDoPassoAPasso: "Em caso de parada cardiorrespiratória em crianças ou gestantes, posicione uma pá no meio do peito e outra na mesma posição, mas nas costas; ", imagemDoPassoAPasso: "12" ),estruturaDosDados(dadosDoPassoAPasso: "Se a vitima estiver molhada, enxugue-a. Se estiver com muitos pêlos, tireo-os com a gilete que já vem com o DEA;", imagemDoPassoAPasso: "13" ),estruturaDosDados(dadosDoPassoAPasso: "Afaste-se da vítima para que o DEA faça a análise do ritmo cardíaco corretamente;", imagemDoPassoAPasso: "14" ),estruturaDosDados(dadosDoPassoAPasso: "Após a análise, o DEA irá decidir se o ritmo da vítima é chocável ou não. Se for, ele irá carregar o choque, senão, ele solicitará que você continue as compressões; ", imagemDoPassoAPasso: "15" ),estruturaDosDados(dadosDoPassoAPasso: "Não esqueça de fazer as compressões enquanto o DEA carrega o choque. Pare somente quando ele disser “choque carregado”;", imagemDoPassoAPasso: "16" ),estruturaDosDados(dadosDoPassoAPasso: "Peça para que todos se afastem e aperte o botão de choque, quando necessário;", imagemDoPassoAPasso: "17" ),estruturaDosDados(dadosDoPassoAPasso: "Reinicie o RCP logo após o choque. A cada 2 minutos o DEA analisa o ritmo da vítima, aproveite esse intervalo para trocar de Socorrista;", imagemDoPassoAPasso: "18" ),estruturaDosDados(dadosDoPassoAPasso: "Pare as compressões somente quando a equipe de socorro chegar ao local e assumir a situação, quando a vítima reagir ou quando o socorrista estiver exausto.", imagemDoPassoAPasso: "19" )]
         
           
            playVi(nome: "rcp")
            passo_atual = rcp_passos
            self.tableview.reloadData()
            myView.removeFromSuperview()
           
   
            
            
            
        }
         
       
        }
        if vias==true{
            if seg.selectedSegmentIndex == 0{
                scrool_introducao()
               
            }
            if seg.selectedSegmentIndex == 1{
                vias_passos = [estruturaDosDados(dadosDoPassoAPasso: "Segure a cabeça do bebê com a palma da mão e apoie seu corpo com o antebraço;", imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Incline levemente a cabeça do neonato para trás e observe sua garganta;", imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Se o que estiver atrapalhando a respiração do neonato for um corpo estranho visível, tente tirar com o dedo mindinho;", imagemDoPassoAPasso: "3"),estruturaDosDados(dadosDoPassoAPasso: "Se o corpo estranho não for eliminado ou não for visível, vire o neonato de barriga para baixo, apoiando a cabeça com suas mãos. Sempre deixe um espaço livre para a boca e nariz;", imagemDoPassoAPasso: "4"),estruturaDosDados(dadosDoPassoAPasso: "Com cuidado, incline o bebê de uma forma que sua cabeça fique mais próximo do chão do que suas pernas;", imagemDoPassoAPasso: "5"),estruturaDosDados(dadosDoPassoAPasso: "Agora que a gravidade está a seu favor, dê pequenos tapas na costa do bebê para tentar eliminar o objeto de sua garganta;", imagemDoPassoAPasso: "6"),estruturaDosDados(dadosDoPassoAPasso: "Se o bebê ficar inconsciente por conta da ausência de respiração, ligue 192 (Samu) e chame por ajuda. Caso não possa pedir o socorro, peça para alguém fazer isso por você;", imagemDoPassoAPasso: "7"),estruturaDosDados(dadosDoPassoAPasso: "Solicite o DEA (Desfribilador Externo Automático) com pás pediátricas;", imagemDoPassoAPasso: "8"),estruturaDosDados(dadosDoPassoAPasso: "Inicie o RCP enquanto o socorro e o DEA não chegam;", imagemDoPassoAPasso: "9"),estruturaDosDados(dadosDoPassoAPasso: "Pare as compressões somente quando a equipe de socorro chegar ao local e assumir a situação, quando o neonato reagir ou quando o socorrista estiver exausto;", imagemDoPassoAPasso: "10")]
                
              

                playVi(nome: "obstru_neo")
                passo_atual = vias_passos
                self.tableview.reloadData()
                myView.removeFromSuperview()
                
            }
            if seg.selectedSegmentIndex == 2{
              playVi(nome: "10")
                vias_passos = [estruturaDosDados(dadosDoPassoAPasso: "Se a pessoa está com uma tosse fraca, respiração e fala difícil, ela pode estar sufocada;", imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Encoraje a pessoa a tossir com força, pois se o que estiver causando o problema estiver perto de sair, nessa hora ele é eliminado;", imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Caso a pessoa continue sufocada, inicia a Manobra de Heimlich. Se a pessoa sufocada for uma criança, faça a manobra de joelhos;", imagemDoPassoAPasso: "3"),estruturaDosDados(dadosDoPassoAPasso: "Coloque-se por trás da vítima, entrelace seus braços em volta dela, ao nível da cintura, e entrelace suas pernas entre as dela;", imagemDoPassoAPasso: "4"),estruturaDosDados(dadosDoPassoAPasso: "Feche uma das mãos e coloque-a com o polegar no abdômen da vítima, um pouco acima do umbigo; ", imagemDoPassoAPasso: "5"),estruturaDosDados(dadosDoPassoAPasso: "Com a outra mão, agarre o punho da mão que já estava acima do umbigo da pessoa e puxe. Faça esse movimento em forma de J, para dentro e para cima na sua direção;", imagemDoPassoAPasso: "6"),estruturaDosDados(dadosDoPassoAPasso: "Repita este movimento até 5 vezes, vigiando se o problema foi resolvido e avaliando o estado de consciência da vítima;", imagemDoPassoAPasso: "7"),estruturaDosDados(dadosDoPassoAPasso: "Não bata nas costas da pessoa para tentar fazer o que estiver preso sair, pois a gravidade pode empurrar o que estiver na garganta mais para baixo e sufocar ainda mais a vítima;", imagemDoPassoAPasso: "8"),estruturaDosDados(dadosDoPassoAPasso: "Pare somente quando o objeto sair da garganta, se a vítima conseguir tossir com força ou na presença de um médico.", imagemDoPassoAPasso: "9")]
              
             
                passo_atual = vias_passos
                self.tableview.reloadData()
                myView.removeFromSuperview()
playVi(nome: "obstruca")
            }
            
        }
     
       
        if afogamento == true {
            if seg.selectedSegmentIndex == 0{
                scrool_introducao()
                
            }
            if seg.selectedSegmentIndex == 1{
                afo_passos = [estruturaDosDados(dadosDoPassoAPasso: "Tire a pessoa que está se afogado da água usando algo grande em que ela possa se agarrar;", imagemDoPassoAPasso: "1"),                                                                                                                                                         estruturaDosDados(dadosDoPassoAPasso: "Se você não alcançar a vítima ou ela estiver em local aberto, como o mar, não pule na água para tentar resgata-la, observe sua posição e chame um salva-vidas ou os Bombeiros;", imagemDoPassoAPasso: "2"),                                                                                              estruturaDosDados(dadosDoPassoAPasso: "Coloque a vitima em posição de drenagem (instruções no vídeo);", imagemDoPassoAPasso: "3"),                                                  estruturaDosDados(dadosDoPassoAPasso: "Cubra a vitima com um cobertor ou algo que possa aquece-la;", imagemDoPassoAPasso: "4"),                                             estruturaDosDados(dadosDoPassoAPasso: "É importante que você não tente retirar água dos pulmões, não dê nada para a vítima comer ou beber e não abandonar a pessoa após a situação;", imagemDoPassoAPasso: "5"),                                                                                                                            estruturaDosDados(dadosDoPassoAPasso: "Se a vitima entrar em parada cardiorrespiratória, ligue para o 192 (Samu) ou peça para que alguém faça isso para você;", imagemDoPassoAPasso: "6"),                                                                                                                              estruturaDosDados(dadosDoPassoAPasso: "Se a vitima entrar em parada cardiorrespiratória, ligue para o 192 (Samu) ou peça para que alguém faça isso para você;", imagemDoPassoAPasso: "7"),                                                                                                                                                          estruturaDosDados(dadosDoPassoAPasso: "Inicie o RCP enquanto o socorro não chega;", imagemDoPassoAPasso: "8"),                                                                                      estruturaDosDados(dadosDoPassoAPasso: "Pare as compressões somente quando a equipe de socorro chegar ao local e assumir a situação, quando a vítima reagir ou quando o socorrista estiver exausto;", imagemDoPassoAPasso: "9"),                                                                                                             estruturaDosDados(dadosDoPassoAPasso: "Ao relatar o ocorrido à um médico, sempre explique como o acidente ocorreu, o tipo e a temperatura da água no momento do afogamento.", imagemDoPassoAPasso: "10")]
                passo_atual = afo_passos
                self.tableview.reloadData()
                playVi(nome: "17")
                myView.removeFromSuperview()
                playVi(nome: "afogamento")
            }
        }
        if sangramento == true {
            
            if seg.selectedSegmentIndex == 0{
                scrool_introducao()
                
                
            }
            if seg.selectedSegmentIndex == 1{
                playVi(nome: "sangramento")
               

                sangra_passos = [estruturaDosDados(dadosDoPassoAPasso: "Se a vítima estiver com ferimentos na pele, lave a ferida com água e sabão para tirar o sangue e qualquer sujeira que possa atrapalhar a cicatrização ou provocar infecções futuras;", imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Se o ferimento for superficial, coloque apenas um curativo por cima;", imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Se a ferida estiver sangrando muito, faça uma compressão direta com um pano limpo ou um curativo compressivo, de forma que isso aperte um pouco a ferida para a vitima não perder muito sangue;", imagemDoPassoAPasso: "3"),estruturaDosDados(dadosDoPassoAPasso: "Caso o ferimento seja muito profundo ou grave, leve a vítima imediatamente à um hospital, após realizar o tamponamento.", imagemDoPassoAPasso: "4")]
                passo_atual = sangra_passos
                self.tableview.reloadData()
                myView.removeFromSuperview()
                
            }
            if seg.selectedSegmentIndex == 2{
                playVi(nome: "17")
                
                sangra_passos = [estruturaDosDados(dadosDoPassoAPasso: "Caso a vitima perca seu dente, segure-o pela coroa e nunca pela raíz;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Dê um pedaço de gase ou um pano limpo para a vítima e peça para ela segurar em cima do ferimento, evitando que o sangue vá direto para sua garganta;",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Evite dar de beber ou comer à vítima;",imagemDoPassoAPasso: "3"),estruturaDosDados(dadosDoPassoAPasso: "Lave o dente com água corrente, nunca esfregando;",imagemDoPassoAPasso: "4"),estruturaDosDados(dadosDoPassoAPasso: "Coloque o dente em um recipiente de leite;",imagemDoPassoAPasso: "5"),estruturaDosDados(dadosDoPassoAPasso: "Procure imediatamente um dentista.",imagemDoPassoAPasso: "6")]

                passo_atual = sangra_passos
                self.tableview.reloadData()
                myView.removeFromSuperview()
                playVi(nome: "sangramento_oral")
            }
            if seg.selectedSegmentIndex == 3{
                playVi(nome: "17")
                
                sangra_passos = [estruturaDosDados(dadosDoPassoAPasso: "Sente a vítima com a cabeça voltada para o chão, deixando o sangue sair pela cavidade e não o contrário;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Peça para vitima respirar pela boca evitando inalar seu próprio sangue;",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Peça para vítima comprimir o nariz por 10 minutos, logo embaixo de seu osso, na cartilagem. Se depois disso o sangramento continuar, peça para a vítima repetir a situação;",imagemDoPassoAPasso: "3"),estruturaDosDados(dadosDoPassoAPasso: "Peça para vítima não tentar falar, tossir ou assoar o nariz, isso pode atrapalhar a coagulação;",imagemDoPassoAPasso: "4"),estruturaDosDados(dadosDoPassoAPasso: "Quando o sangramento estiver sob controle, na mesma posição, limpe delicadamente em volta do nariz e da boca com água em temperatura ambiente;",imagemDoPassoAPasso: "5"),estruturaDosDados(dadosDoPassoAPasso: "Não dê bebida ou comida para a vítima e nem use qualquer tipo de droga na mesma;",imagemDoPassoAPasso: "6"),estruturaDosDados(dadosDoPassoAPasso: "Oriente a vítima a descansar por algumas horas e não fazer esforço;",imagemDoPassoAPasso: "7"),estruturaDosDados(dadosDoPassoAPasso: "Se o sangramento persistir por mais de 30 minutos, leve a vítima para o hospital na posição dita anteriormente.",imagemDoPassoAPasso: "8")]
                myView.removeFromSuperview()
                passo_atual = sangra_passos
                self.tableview.reloadData()
                playVi(nome: "sangramento_na")
            }
            if seg.selectedSegmentIndex == 4{
                playVi(nome: "17")
                print("Orelha")
                playVi(nome: "sangramento_orelha")
                
                sangra_passos = [estruturaDosDados(dadosDoPassoAPasso: "Incline a cabeça da vítima para o lado da orelha que está sangrando, dessa forma o sangue sai pela cavidade e não o contrário;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Deixe uma gase ou um pano limpo perto das orelhas para que o sangue não caia no chão ou nas roupas. É importante que você nunca atrapalhe a saída do sangue com a gase ou com o pano;",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Leve a vítima ao hospital imediatamente.",imagemDoPassoAPasso: "3")]
                
                passo_atual = sangra_passos
                self.tableview.reloadData()
                myView.removeFromSuperview()
            }
        }
        if queimadura == true {
            playVi(nome: "17")
            print("oi")
            if seg.selectedSegmentIndex == 0{
                scrool_introducao()
                
            }
            if seg.selectedSegmentIndex == 1{
                
                queimadu_passos = [estruturaDosDados(dadosDoPassoAPasso: "Pare o agente causador da queimadura, seja ele eletricidade ou fogo. Se a queimadura for do tipo química, ligue para o CIT;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Lave a lesão da vítima com água corrente por 5 minutos.",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Cubra cuidadosamente a região com um pano limpo e seco, sem esfregar a região;",imagemDoPassoAPasso: "3"),estruturaDosDados(dadosDoPassoAPasso: "Nunca coloque manteiga, pasta de dente, óleo, pó de café ou qualquer outro produto sobre a queimadura. Além de grudar na pele, eles podem causar alergia e agravar o quadro;",imagemDoPassoAPasso: "4"),estruturaDosDados(dadosDoPassoAPasso: "Leve a vítima ao hospital.",imagemDoPassoAPasso: "5")]
                passo_atual = queimadu_passos
                self.tableview.reloadData()
                myView.removeFromSuperview()
                playVi(nome: "queimadura")
            }
            if seg.selectedSegmentIndex == 2{
                playVi(nome: "17")
                
                queimadu_passos = [estruturaDosDados(dadosDoPassoAPasso: "Pare o agente causador da queimadura, seja ele eletricidade ou fogo. Se a queimadura for do tipo química, ligue para o CIT;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Lave a lesão da vítima com água corrente por 5 minutos.",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Se as bolhas estiverem intactas, cubra a área da queimadura com gaze ou um pano limpo. É importante que você nunca estoure as bolhas;",imagemDoPassoAPasso: "3"),estruturaDosDados(dadosDoPassoAPasso: "Evite tirar roupas que grudaram na pele da vítima para não arranca-la sem querer. Deixe um profissional fazer isso;",imagemDoPassoAPasso: "4"),estruturaDosDados(dadosDoPassoAPasso: "Nunca coloque manteiga, pasta de dente, óleo, pó de café ou qualquer outro produto sobre a queimadura. Além de grudar na pele, eles podem causar alergia e agravar o quadro;",imagemDoPassoAPasso: "5"),estruturaDosDados(dadosDoPassoAPasso: "Não coloque gelo na queimadura, ao contrário do que muitos pensam, isso apenas piora a situação;",imagemDoPassoAPasso: "6"),estruturaDosDados(dadosDoPassoAPasso: "Leve a vítima ao hospital.",imagemDoPassoAPasso: "7")]

                passo_atual = queimadu_passos
                self.tableview.reloadData()
                myView.removeFromSuperview()
                
            }
            if seg.selectedSegmentIndex == 3{
                playVi(nome: "17")
                
                queimadu_passos = [estruturaDosDados(dadosDoPassoAPasso: "Pare o agente causador da queimadura, seja ele eletricidade ou fogo. Se a queimadura for do tipo química, ligue para o CIT;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Lave a lesão da vítima com água corrente por 5 minutos.",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Nunca coloque manteiga, pasta de dente, óleo, pó de café ou qualquer outro produto sobre a queimadura. Além de grudar na pele, eles podem causar alergia e agravar o quadro;",imagemDoPassoAPasso: "3"),estruturaDosDados(dadosDoPassoAPasso: "Cubra a região com um pano limpo;",imagemDoPassoAPasso: "4"),estruturaDosDados(dadosDoPassoAPasso: "Leve a vítima para o hospital imediatamente;",imagemDoPassoAPasso: "5")]

                passo_atual = queimadu_passos
                self.tableview.reloadData()
                myView.removeFromSuperview()
                
            }
        }
        if fratura == true {
            if seg.selectedSegmentIndex == 0{
                scrool_introducao()
                
                
            }
            if seg.selectedSegmentIndex == 1{
                fratura_passos = [estruturaDosDados(dadosDoPassoAPasso: "No caso de entorses (rompimento de tendões ou ligamentos) aplique compressas com gelo na área da articulação e leve a vítima ao hospital;",imagemDoPassoAPasso: "1"),                                                                                                                      estruturaDosDados(dadosDoPassoAPasso: "No caso de luxações (quando o osso sofre um movimento brusco e desloca-se do seu lugar original) imobilize a região afetada e leve a vítima ao hospital;",imagemDoPassoAPasso: "2"),                                                                                                                     estruturaDosDados(dadosDoPassoAPasso: "No caso de fraturas, imobilize as articulações acima e abaixo do membro ou região lesionada procurando sempre movimentar o mínimo possível a área afetada e leve a vítima ao hospital;",imagemDoPassoAPasso: "3"),                                                                                                   estruturaDosDados(dadosDoPassoAPasso: "No caso de fratura do pescoço e coluna não mexa a vítima e diga para ela não se mexer também, mesmo que tenha dor. Esse procedimento diminuirá o risco de lesões graves;",imagemDoPassoAPasso: "4"),                                                                                                                     estruturaDosDados(dadosDoPassoAPasso: "No caso de fratura exposta, em que o osso perfura a pele, cubra o ferimento com um pano limpo;",imagemDoPassoAPasso: "5"),                                                                                                                                                               estruturaDosDados(dadosDoPassoAPasso: "Se houver sangramento muito intenso, faça compressões na região fraturada com panos limpos;",imagemDoPassoAPasso: "6"),                                                                                                                                                              estruturaDosDados(dadosDoPassoAPasso: "Não puxe e nem coloque no lugar o osso fraturado ou deslocado, não massageie a área e não dê bebida ou comida para vítima.",imagemDoPassoAPasso: "7")]
                passo_atual = fratura_passos
                self.tableview.reloadData()
                playVi(nome: "17")
                myView.removeFromSuperview()
                playVi(nome: "fratura")
            }
            
        }
        if convulsao == true {
            if seg.selectedSegmentIndex == 0{
                scrool_introducao()
            }
            if seg.selectedSegmentIndex == 1{
                convulsao_passos = [estruturaDosDados(dadosDoPassoAPasso: "Mantenha a calma e marque o horário que a convulsão iniciou;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Posicione a vítima em um local seguro, afastando móveis e objetos que possam machuca-la;",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Apoie somente a cabeça da vítima, evitando segurar seu corpo. Isso pode mais machucar a que ajudar. É importante que você deixe a convulsão acontecer;",imagemDoPassoAPasso: "3"),                                                                                         estruturaDosDados(dadosDoPassoAPasso: "Afaste curiosos dando espaço para a vítima debater-se sem machucar ninguém ao redor;",imagemDoPassoAPasso: "4"),estruturaDosDados(dadosDoPassoAPasso: "Nunca puxe a língua da vítima para fora, ela não estará controlando seus músculos e pode acabar arrancando ou machucando seu dedo;",imagemDoPassoAPasso: "5"),                                                                                                                                estruturaDosDados(dadosDoPassoAPasso: "Fique junto da vítima até que ela recupere a consciência, anotando o tempo que ela permaneceu convulsionando;",imagemDoPassoAPasso: "6"),                                                                                                                    estruturaDosDados(dadosDoPassoAPasso: "Novas convulsões podem ocorrer então esteja de olho no relógio, sempre marcando a duração do evento;",imagemDoPassoAPasso: "7"),                                                                                                                             estruturaDosDados(dadosDoPassoAPasso: "Se a vítima estiver tendo a primeira crise de convulsão, ligue 192 (Samu) e chame socorro;",imagemDoPassoAPasso: "8"),                                                                                                                                                   estruturaDosDados(dadosDoPassoAPasso: "Se a duração da crise for superior a cinco minutos, ligue 192 (Samu) e chame socorro;",imagemDoPassoAPasso: "9"),                                                                                                                                                                    estruturaDosDados(dadosDoPassoAPasso: "Se houver a ocorrência de duas ou mais crises em menos de uma hora, sem retorno de consciência entre elas, ligue 192 (Samu) e chame socorro;",imagemDoPassoAPasso: "10"),                                                                                                                                                 estruturaDosDados(dadosDoPassoAPasso: "Chame o socorro também se a vítima sofrer algum trauma significativo durante a convulsão.",imagemDoPassoAPasso: "11")]
                passo_atual = fratura_passos
                passo_atual = convulsao_passos
                self.tableview.reloadData()
                playVi(nome: "convul")
                myView.removeFromSuperview()
                
            }
            
        }
        if avc == true{
            if seg.selectedSegmentIndex == 0{
                scrool_introducao()
                
            }
            if seg.selectedSegmentIndex == 1{
                avc_passos = [estruturaDosDados(dadosDoPassoAPasso: "Peça para a vítima sorrir. Se um dos lados da sua boca não levantarem, ligue 192 (Samu) e chame o socorro;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Pergunte a vitima coisas simples como “Qual seu nome?”, “Que dia é hoje?”. Se a vítima falar enrolado ou responder de maneira desconexa, ligue 192 (Samu) e chame o socorro;",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Peça para a vítima fechar os olhos e levantar os dois braços. Se apenas um braço levantar ou os dois levantarem, mas um deles cair logo em seguida, ligue 192 (Samu) e chame o socorro.",imagemDoPassoAPasso: "3")]
                passo_atual = avc_passos
                self.tableview.reloadData()
                playVi(nome: "17")
                print("entrou aqui")
                myView.removeFromSuperview()
            }
            
           
        }
        if intox == true {
            if seg.selectedSegmentIndex == 0{
                scrool_introducao()
                
            }
            if seg.selectedSegmentIndex == 1{
                intox_passos = [estruturaDosDados(dadosDoPassoAPasso: "Não ofereça comida ou bebida à vítima;",imagemDoPassoAPasso: "1"),estruturaDosDados(dadosDoPassoAPasso: "Não induza o vômito da vítima;",imagemDoPassoAPasso: "2"),estruturaDosDados(dadosDoPassoAPasso: "Ligue para 0800 722 6001 (CIT). Somente especialistas saberão o que fazer, dando à vítima ou ao socorrista informações sobre qual procedimento seguir;",imagemDoPassoAPasso: "3")]
                passo_atual = intox_passos
                self.tableview.reloadData()
                playVi(nome: "17")
                myView.removeFromSuperview()
                playVi(nome: "intox")
                
            }
        
        }


        
        
        
    }
 
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myView.showsVerticalScrollIndicator = true
        myView.isScrollEnabled = true
        //myView.alpha = 0.5
        myView.tag = 100
        myView.isUserInteractionEnabled = true
        myView.backgroundColor = UIColor.white
        ConteudoInf = conteudo[informacao]
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 250, height: 30))
        label.text = ConteudoInf[0]
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
        
        seg.tintColor = UIColor(netHex: 0x890006)
        
        
        
        ///teste para resolver o bug rodar
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
     
        
        
        switch informacao {
        case 0:
            seg.setTitle("Introdução", forSegmentAt: 0)
            seg.setTitle("Neonato", forSegmentAt: 1)
            seg.insertSegment(withTitle: "Adulto", at: 2, animated: false)
           //playVi(nome: nome_video)
            rcp = true
            vias = false
             afogamento = false
             sangramento = false
             queimadura = false
             fratura = false
             avc = false
             convulsao = false
             intox = false
             passo_atual = rcp_passos
         kitt = false
          scrool_introducao()
            
           



            
            
        case 1:
            seg.setTitle("Introdução", forSegmentAt: 0)
            seg.setTitle("Neonato", forSegmentAt: 1)
            seg.insertSegment(withTitle: "Adulto", at: 2, animated: false)
            vias = true
             rcp = false
            kitt = false
             afogamento = false
             sangramento = false
             queimadura = false
             fratura = false
             avc = false
             convulsao = false
             intox = false
         scrool_introducao()
         passo_atual = vias_passos
            
            
            
          
        case 2:
            afogamento = true
            seg.setTitle("Introdução", forSegmentAt: 0)
            seg.setTitle("Afogamento", forSegmentAt: 1)
            vias = false
            rcp = false
            afogamento = true
            sangramento = false
            queimadura = false
            fratura = false
            avc = false
            convulsao = false
            intox = false
            passo_atual = afo_passos
            kitt = false
            scrool_introducao()
        case 3:
            rcp = false
            kitt = false
            vias = false
            afogamento = true
            sangramento = true
            queimadura = false
            fratura = false
            avc = false
            convulsao = false
            intox = false
            passo_atual = sangra_passos
            seg.setTitle("Intro", forSegmentAt: 0)
            seg.setTitle("Pele", forSegmentAt: 1)
            seg.insertSegment(withTitle: "Oral", at: 2, animated: false)
            seg.insertSegment(withTitle: "Nasal", at: 3, animated: false)
            seg.insertSegment(withTitle: "Orelha", at: 4, animated: false)
            scrool_introducao()
            
            
        case 4:
            rcp = false
            vias = false
            afogamento = true
            sangramento = false
            queimadura = true
            fratura = false
            avc = false
            convulsao = false
            intox = false
            passo_atual = queimadu_passos
            seg.setTitle("Introdução", forSegmentAt: 0)
            seg.setTitle("1 grau", forSegmentAt: 1)
            seg.insertSegment(withTitle: "2 grau", at: 2, animated: false)
            seg.insertSegment(withTitle: "3 grau", at: 3, animated: false)
            kitt = false
            scrool_introducao()
            
        case 5:
            kitt = false
            rcp = false
            vias = false
            afogamento = false
            sangramento = false
            queimadura = false
            fratura = true
            avc = false
            convulsao = false
            intox = false
            passo_atual = fratura_passos
            seg.setTitle("Introdução", forSegmentAt: 0)
            seg.setTitle("Fratura", forSegmentAt: 1)
            scrool_introducao()
        case 6:
            seg.setTitle("Introdução", forSegmentAt: 0)
            seg.setTitle("Convulsão", forSegmentAt: 1)
            kitt = false
            scrool_introducao()
            
            rcp = false
            vias = false
            afogamento = false
            sangramento = false
            queimadura = false
            fratura = false
            avc = false
            convulsao = true
            intox = false
            passo_atual = convulsao_passos
        case 7:
            rcp = false
            vias = false
            afogamento = false
            sangramento = false
            queimadura = false
            fratura = false
            avc = false
            convulsao = false
            intox = true
            passo_atual = intox_passos
            seg.setTitle("Introdução", forSegmentAt: 0)
            seg.setTitle("Intoxicação", forSegmentAt: 1)
            kitt = false
            scrool_introducao()
          
        case 8:
            rcp = false
            vias = false
            afogamento = false
            sangramento = false
            queimadura = false
            fratura = false
            avc = true
            convulsao = false
            intox = false
            passo_atual = avc_passos
            seg.setTitle("Introdução", forSegmentAt: 0)
            seg.setTitle("AVC", forSegmentAt: 1)
            kitt = false
            scrool_introducao()
            
        case 9: 
            print("kit")
//            seg.setTitle("Kit", forSegmentAt: 0)
//            seg.setTitle("dois", forSegmentAt: 1)
//      
//                        kitt = true
//            scrool_introducao()
            
        default:
            print("default")
        }
        
        
        
        
        
        
        
    }
    
    
//     func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutableRawPointer) {
//        if keyPath == "rate" {
//            if let rate = change?[NSKeyValueChangeNewKey] as? Float {
//                if rate == 0.0 {
//                    print("playback stopped")
//                }
//                if rate == 1.0 {
//                    print("normal playback")
//                }
//                if rate == -1.0 {
//                    print("reverse playback")
//                }
//            }
//        }
//    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func embora(_ sender: Any) {
        self.dismiss(animated: true, completion:nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   
  
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Change the selected background view of the cell.
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return passo_atual.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CellItens

        cell.conteudoCelula?.text = passo_atual[indexPath.row].dadosDoPassoAPasso
        cell.conteudoCelula?.textColor = UIColor(netHex: 0x737372)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.ImageCelula?.image = UIImage(named: passo_atual[indexPath.row].imagemDoPassoAPasso)

                return cell
    }
    
    private func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeLeft
    }
    private func shouldAutorotate() -> Bool {
        return true
    }
    
    
    
}
