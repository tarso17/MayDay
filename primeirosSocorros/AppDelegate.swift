//
//  AppDelegate.swift
//  primeirosSocorros
//
//  Created by Thiago Bessa on 07/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var conteudo = ["Urinar sobre queimadura de água-viva não ajuda na cicatrização.",
                    "Chupar veneno de cobra após uma picada não tira parte do veneno da corrente sanguínea.",
                    "Tomar uma dose de whisky não alivia a dor de dente. O efeito analgésico está relacionado ao gelo e não ao álcool.",
                    "Passar pasta de dente sobre queimaduras não neutraliza a dor.",
                    "Colocar bife cru no olho roxo não alivia a dor do machucado. Aposte sempre na boa e velha bolsa de gelo.",
                    "Água oxigenada em cortes e feridas não tem efeito contra os germes. O ideal é limpar os machucados com água e sabão.",
                    "Extrair ferrão de abelha com os dedos não impede de espalhar o veneno na corrente sanguínea.",
                    "Evite posicionar a cabeça para trás para evitar ou conter sangramentos nasais. O ideal é deixar o sangue sair da cavidade, apertando apenas a cartilagem.",
                    "Pacientes com ataque cardíaco não devem ficar deitados. O ideal é sentar e flexionar levemente os joelhos apoiando a cabeça.",
                    "Não é indicado que uma pessoa sentindo fraqueza coloque sua cabeça entre as pernas. O ideal é que ela se deite e levante as pernas para cima, aumentando o fluxo sanguíneo no cérebro.",
                    "Meio de fortuna para maca: Dois canos ou varas compridas e roupas com mangas.",
                    "Meio de fortuna para maca: Uma porta.",
                    "Meio de fortuna para maca: Dois canos ou varas compridas e cipó ou corda entrelaçados formando uma rede",
                    "Meio de fortuna para maca: Dois canos ou varas compridas e um cobertor ou pano grande amarrados.",
                    "Meio de fortuna para fratura: Revista ou papelão que imobilize o membro.",
                    "Meio de fortuna para fratura no dedo: Palito de picolé e esparadrapo imobilizando a fratura.",
                    "Meio de fortuna para luvas de látex: Saco plástico.",
                    "Nunca retire o capacete de uma vítima em um acidente. Isso pode machucar ou piorar uma fratura de cervical, por exemplo."]
    let rand = Int(arc4random_uniform(22))
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {granted, error in
            //handle error here
            if granted{
                
            }
        })

        
        
        
        //NOTIFICATION
        
        let content = UNMutableNotificationContent()
        content.title = "Notificação"
        content.subtitle = "Curiosidade"
        //content.body = String(describing: conteudo[rand])
        content.badge = 1
        
        //TRIGGER
        
        var dataComponents = DateComponents()
        dataComponents.hour = 14
        dataComponents.minute = 00
//        let trigger =  UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                let trigger =  UNCalendarNotificationTrigger(dateMatching: dataComponents, repeats: false)
        let requestIdentifier = "Queez"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: {error in
            // handle error
        })
        

        
        
        
        return true
        
        
        
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
//    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
//        guard let vc = (window?.rootViewController?.presentedViewController) else {
//            return .portrait
//        }
//        
//        if (vc.isKind(of: NSClassFromString("infoConteudoController")!)) {
//            return .allButUpsideDown
//            
//        }
//       
//        
//        
//        return .portrait
//    }



}

