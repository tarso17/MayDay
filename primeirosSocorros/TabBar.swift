//
//  TabBar.swift
//  primeirosSocorros
//
//  Created by Carlos  Machado on 24/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import Foundation
import UIKit

class Tabbar: UITabBarController, UINavigationControllerDelegate{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let statusBar = UIStatusBarStyle.lightContent
        
        
        let colorBar = UINavigationBar.appearance()
        colorBar.barTintColor = UIColor(netHex:0x890006)
        colorBar.tintColor = UIColor.white
        //
        let Tbar = UITabBar.appearance()
        let color1 = UIColor(red: 148, green: 0x25, blue: 0x30)
        let color2 = UIColor.white
        
        Tbar.tintColor = color1
        Tbar.barTintColor = color2
        
    }
    
    
    
    
}



extension UIColor{
    
    convenience init(red : Int, green: Int, blue: Int) { // IMPLEMENTA O RGB POR PARTES
        
        assert(red >= 0 && red <= 255 , "Invalid red component")
        assert(green >= 0 && green <= 255 , "Invalid green component")
        assert(blue >= 0 && blue <= 255 , "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
        print("sdssd")
        
    }
    
    convenience init(netHex: Int) {//IMPLEMENTA O HEXADECIMAL POR INTEIRO.
        
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
        
    }
    
}

