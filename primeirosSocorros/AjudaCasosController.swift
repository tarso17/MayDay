//
//  AjudaCasosController.swift
//  primeirosSocorros
//
//  Created by Thiago Bessa on 02/12/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit

class AjudaCasosController: UIViewController {

 
   
    @IBOutlet weak var botaoComecar: UIButton!
    @IBOutlet weak var imgTela: UIImageView!
    @IBOutlet weak var pages: UIPageControl!
    var x: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        x = 0
        passar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func passar(){
        
        switch x {
        case 0:
            pages.currentPage = x
            imgTela.image = UIImage(named: "tutorialCasos1")
            x = 1
            
        case 1:
            pages.currentPage = x
            imgTela.image = UIImage(named: "tutorialCasos2")
            botaoComecar.isHidden = false

            x = 0
        default:
            break
            
        }
    }
    
    @IBAction func ShowPassar(_ sender: Any) {
        passar()
    }
    @IBAction func ShowVoltar(_ sender: UIButton) {
        self.dismiss(animated: true, completion:nil)

    }
    
}
