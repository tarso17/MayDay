//
//  EmergenciaController.swift
//  primeirosSocorros
//
//  Created by Thiago Bessa on 10/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit

class EmergenciaController: UIViewController, UIViewControllerTransitioningDelegate {
    @IBOutlet weak var BOMB: UIButton!
    @IBOutlet weak var DEFES: UIButton!
    @IBOutlet weak var CIT: UIButton!
    @IBOutlet weak var SAMU: UIButton!
    @IBOutlet weak var POLI: UIButton!
    
    var selected: UIButton!

    var transition = CircularTransition()
    
    var show: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 250, height: 30))
        label.text = "Emergência"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
        
        
        
        BOMB.layer.cornerRadius = BOMB.frame.size.width/2
        DEFES.layer.cornerRadius = DEFES.frame.size.width/2
        CIT.layer.cornerRadius = CIT.frame.size.width/2
        SAMU.layer.cornerRadius = SAMU.frame.size.width/2
        POLI.layer.cornerRadius = POLI.frame.size.width/2



        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func ShowPm(_ sender: UIButton) {
        show = 0
        self.selected = sender
        performSegue(withIdentifier: "segue", sender: nil)
    }
    
    
    
    @IBAction func SamuShow(_ sender: UIButton) {
        show = 1
        self.selected = sender
        performSegue(withIdentifier: "segue", sender: nil)
    }
    
    @IBAction func bombShow(_ sender: UIButton) {
        
        show = 2
        self.selected = sender
        performSegue(withIdentifier: "segue", sender: nil)
        
    }
    @IBAction func defesShow(_ sender: UIButton) {
        show = 3
        self.selected = sender
        performSegue(withIdentifier: "segue", sender: nil)

    }
    @IBAction func citShow(_ sender: UIButton) {
        show = 4
        self.selected = sender
        performSegue(withIdentifier: "segue", sender: nil)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination
        secondVC.transitioningDelegate = self
        secondVC.modalPresentationStyle = .custom
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        
        let x: InfoEmegController = segue.destination as! InfoEmegController
        
        x.canal = show
   }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.startingPoint = self.selected.center
        transition.circleColor = POLI.backgroundColor!
        
        return transition
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = self.selected.center
        transition.circleColor = POLI.backgroundColor!
        
        return transition
    }
   
}
