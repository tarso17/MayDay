//
//  AjudaTreinoController.swift
//  primeirosSocorros
//
//  Created by Thiago Bessa on 02/12/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit

class AjudaTreinoController: UIViewController {

    @IBOutlet weak var pages: UIPageControl!
    @IBOutlet weak var imgTela: UIImageView!
    @IBOutlet weak var butComecar: UIButton!
    
    var x:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        x = 0
        passar()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    func passar(){
        
        switch x {
        case 0:
            pages.currentPage = x
            imgTela.image = UIImage(named: "Tutorial1")
            x = 1
            
        case 1:
            pages.currentPage = x
            imgTela.image = UIImage(named: "Tutorial2")
            x = 0
            butComecar.isHidden = false
        default:
            break
            
        }
        
    }
    
    @IBAction func ShowImagens(_ sender: UIButton) {
        passar()
    }
    
    @IBAction func ShowIr(_ sender: Any) {
        self.dismiss(animated: true, completion:nil)
                }

}

