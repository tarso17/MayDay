//
//  InfoEmegController.swift
//  primeirosSocorros
//
//  Created by Thiago Bessa on 16/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit

class InfoEmegController: UIViewController {

   // @IBOutlet weak var textoEmeg: UILabel!
    @IBOutlet weak var imageEmerg: UIImageView!
    @IBOutlet weak var ligarEmerg: UIButton!

    
    var canal: Int!
    
    var conteudo = [["Polícia","tel://190","A Polícia Militar é a polícia fardada responsável pela segurança da população e por impedir que crimes ocorram. Como forma de prevenção, a polícia militar  circula por lugares públicos, buscando sempre garantir a paz e a tranquilidade das pessoas. Quando necessário, a  também deve perseguir criminosos, efetuando prisões, desde que estejam de acordo com a lei. Em situações de grande concentração de pessoas, age orientando-as e antecipando-se aos problemas.","190","- Em casos suspeitos, a Polícia pode abordar e revistar sujeitos para proteger os cidadãos;-No caso de crime, a Polícia pode prender sujeitos, desde que em flagrante ou com ordem do juiz;-Em caso de ameaças, a Polícia pode empregar força e usar armas de fogo para proteger os cidadãos.",
                     "policia","infPoliciaMilitar"],
                    ["SAMU","tel://192","O Serviço de Atendimento Móvel de Urgência – SAMU tem como objetivo chegar precocemente à vítima após ter ocorrido alguma situação de urgência ou emergência de natureza clínica, cirúrgica, traumática, obstétrica, pediátrica, psiquiátrica, entre outras, que possa levar a sofrimento, a sequelas ou mesmo a morte. É um serviço gratuito, que funciona 24 horas, por meio da prestação de orientações e do envio de veículos tripulados por equipe capacitada, acessado pelo número “192” e acionado por uma Central de Regulação das Urgências. O SAMU realiza os atendimentos em qualquer lugar: residências, locais de trabalho e vias públicas, e conta com equipes que reúne médicos, enfermeiros, auxiliares de enfermagem e condutores socorristas.","192","- Na ocorrência de problemas cardiorrespiratórios; -Em casos de Intoxicação exógena; -Em caso de queimaduras graves; - Na ocorrência de maus tratos; - Em trabalhos de parto onde haja risco de morte da mãe ou do feto; - Em casos de tentativas de suicídio; - Em crises hipertensivas; - Quando houver acidentes/traumas com vítimas; - Em casos de afogamentos; - Em casos de choque elétrico; - Em acidentes com produtos perigosos;","samu","infSAMU"],
                    ["Corpo de Bombeiros","tel://193","O Corpo de Bombeiros tem como principal objetivo a execução de atividades de Defesa Civil, Prevenção e Combate a Incêndios, Buscas, Salvamentos e Socorros Públicos. Possuem uma grande preocupação em melhorar o atendimento pré-hospitalar, sendo os primeiros a chegarem ao local do acidente, assim como o SAMU.","193","- Em serviço de Guarda Vidas; - Em combate a incêndios florestais; - Em salvamento aquático; - No caso de resgate em altura; - No caso de resgate em montanha; - Em intervenção de incidentes com produtos perigosos, tais como: inflamáveis e substâncias tóxicas; - No caso de vistorias técnicas das condições de segurança em edificações, estádios, ou qualquer outro local de grande concentração de público; - Em serviço de Atendimento Pré-Hospitalar.","bombeiro","infCorpoDeBombeiros"],
                    ["Defesa Civil","tel://199","A defesa civil ou proteção civil é o conjunto de ações preventivas, de socorro e assistenciais destinadas a evitar ou minimizar os desastres naturais e os incidentes tecnológicos, preservando a integridade da população e restabelecendo a normalidade social.","199","- No caso de graves desastres com vítimas e desabrigados; - Em acidentes rodoviários, ferroviários, metroviários, envolvendo grande número de pessoas; - No caso de inundações; - No caso de grandes incêndios, com vítimas; - Em acidentes com combustíveis, produtos perigosos (radioativos, químicos, inflamáveis, tóxicos, explosivos e corrosivos); -  No caso de explosões em depósitos de gás de cozinha; - No caso de rachaduras, trincas e fissuras em edificações; - No caso de deformações em estruturas (lajes, vigas, pilares e paredes); - No caso de infiltrações graves com grande risco de desabamento; - No caso de recalque de fundações (rebaixamentos da terra ou da parede).","civil","infDefesaCivil"],
                    ["CIT","tel://08007226001","O Centro de Informações Toxicológicas do Amazonas (CIT/AM) é um serviço 24h do Hospital Universitário Getúlio Vargas (HUGV)/ Universidade Federal do Amazonas (UFAM), que presta informações toxicológicas gratuitamente via telefone a qualquer pessoa, seja profissional da saúde ou cidadão comum.","0800 722 6001","- No caso de orientação sobre prevenção a riscos toxicológicos, como auto-medicação, uso de praguicidas, produtos de limpeza e plantas e animais  venenosos; - No tratamento ou diagnóstico em casos de envenenamento por substâncias tóxicas de qualquer espécie.","cit","infCIT"]
                    ]
    var Conteudoshow: [String]!

   @IBOutlet weak var imagemTexto: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Conteudoshow = conteudo[canal]
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 250, height: 30))
        label.text = Conteudoshow[0]
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
        imagemTexto.image = UIImage(named: Conteudoshow[6])
        imagemTexto.sizeToFit()
        
        //        textoInf.adjustsFontSizeToFitWidth = true
        
      //  textoInf.sizeToFit() Nao apague isso por favor - Bessa///
        
        
        imageEmerg.image = UIImage(named: Conteudoshow[5])
        imageEmerg.layer.cornerRadius = imageEmerg.frame.size.width/2
        ligarEmerg.layer.cornerRadius = ligarEmerg.frame.size.width/8
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }


    @IBAction func ShowLigar(_ sender: Any) {
        let alert = UIAlertController(title: "Quer mesmo ligar?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Não", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { action in
            switch action.style{
            case .default:
                 UIApplication.shared.open(NSURL(string: "\(self.Conteudoshow[1])") as! URL)
                print("entrou aqui")
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))

        self.present(alert, animated: true, completion: nil)
        
       
    }
    
}
