//
//  ConteudoController.swift
//  primeirosSocorros
//
//  Created by Thiago Bessa on 12/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit

class ConteudoController: UIViewController, UIViewControllerTransitioningDelegate {

    var transition = CircularTransition()
    
    @IBOutlet weak var conteudo1: UIButton!
   
    @IBOutlet weak var conteudo2: UIButton!
    
    @IBOutlet weak var conteudo3: UIButton!
    
    @IBOutlet weak var conteudo4: UIButton!
    
    @IBOutlet weak var conteudo5: UIButton!
    
    @IBOutlet weak var conteudo6: UIButton!
    
    @IBOutlet weak var conteudo7: UIButton!
    
    @IBOutlet weak var conteudo8: UIButton!
    @IBOutlet weak var conteudo9: UIButton!
    @IBOutlet weak var conteudo10: UIButton!
    
    var selected: UIButton!
    var inform: Int!
    var verificar: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 215, height: 30))
        label.text = "Primeiros Socorros"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
        
        conteudo1.layer.cornerRadius = conteudo1.frame.size.width/2
        conteudo2.layer.cornerRadius = conteudo2.frame.size.width/2
        conteudo3.layer.cornerRadius = conteudo3.frame.size.width/2
        conteudo4.layer.cornerRadius = conteudo4.frame.size.width/2
        conteudo5.layer.cornerRadius = conteudo5.frame.size.width/2
        conteudo6.layer.cornerRadius = conteudo6.frame.size.width/2
        conteudo7.layer.cornerRadius = conteudo7.frame.size.width/2
        conteudo8.layer.cornerRadius = conteudo8.frame.size.width/2
        conteudo9.layer.cornerRadius = conteudo9.frame.size.width/2
        conteudo10.layer.cornerRadius = conteudo10.frame.size.width/2
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination
        secondVC.transitioningDelegate = self
        secondVC.modalPresentationStyle = .custom
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        
        if verificar{
        let InfoConteudo: InfoConteudoController = segue.destination as! InfoConteudoController
        
        InfoConteudo.informacao = inform

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func conteudoShow1(_ sender: UIButton) {
        inform = 0
        verificar = true
        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)

    }
    
    @IBAction func conteudoShow2(_ sender: UIButton) {
        inform = 1
        verificar = true

        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)
    }
    @IBAction func conteudoShow3(_ sender: UIButton) {
        inform = 2
        verificar = true

        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)

    }
    @IBAction func conteudoShow4(_ sender: UIButton) {
        inform = 3
        verificar = true

        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)

    }
    @IBAction func conteudoShow5(_ sender: UIButton) {
        inform = 4
        verificar = true

        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)

    }
    @IBAction func conteudoShow6(_ sender: UIButton) {
        inform = 5
        verificar = true

        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)

    }
    @IBAction func conteudoShow7(_ sender: UIButton) {
        inform = 6
        verificar = true

        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)

    }
    @IBAction func conteudoShow8(_ sender: UIButton) {
        inform = 7
        verificar = true

        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)

    }
    
    @IBAction func conteudoShow9(_ sender: UIButton) {
        inform = 8
        verificar = true

        self.selected = sender
        performSegue(withIdentifier: "segue1", sender: nil)
    }
    @IBAction func conteudoShow10(_ sender: UIButton) {
        verificar = false

        self.selected = sender
    }
    
    
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.startingPoint = selected.center
        transition.circleColor = conteudo1.backgroundColor!
        
        return transition
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = selected.center
        transition.circleColor = conteudo1.backgroundColor!
        
        return transition
    }

}
