//
//  TreinoController.swift
//  primeirosSocorros
//
//  Created by Carlos  Machado on 02/12/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import Foundation
import UIKit

class TreinoController: UIViewController{
    var numero = 0
    @IBOutlet weak var vaii: UIButton!
    
    @IBOutlet weak var novamente: UIButton!
    
   // @IBOutlet weak var botao1: UIButton!
    @IBOutlet weak var butao: UIButton!
    
    @IBAction func ButtonQueOSauloQuer(_ sender: Any) {
        
        switch numero {
        case 0:
            print("entrou case 0")
            
            UIView.transition(with: sender as! UIView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.vaii.setImage(UIImage(named: "Questao1"), for: .normal)
                
            }, completion: nil)
            
            self.butao.isHidden = true
            self.novamente.isHidden = true
            
            
            
        case 1:
            
            UIView.transition(with: sender as! UIView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.vaii.setImage(UIImage(named: "Questao2"), for: .normal)
                
            }, completion: nil)
            self.novamente.isHidden = true

            
            self.butao.isHidden = true
        case 2:
            
            
            
            UIView.transition(with: sender as! UIView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.vaii.setImage(UIImage(named: "Questao3"), for: .normal)
            }, completion: nil)
            self.butao.isHidden = true
            self.novamente.isHidden = true

        case 3:
            UIView.transition(with: sender as! UIView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.vaii.setImage(UIImage(named: "Questao4"), for: .normal)
            }, completion: nil)
            self.butao.isHidden = true
            self.novamente.isHidden = true

        case 4:
            UIView.transition(with: sender as! UIView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.vaii.setImage(UIImage(named: "Questao5"), for: .normal)
            }, completion: nil)
            self.butao.isHidden = true
            self.novamente.isHidden = true

            
        case 5:
            UIView.transition(with: sender as! UIView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.vaii.setImage(UIImage(named: "Questao5"), for: .normal)
            }, completion: nil)
            self.butao.isHidden = true
            self.novamente.isHidden = true

        case 6:
            UIView.transition(with: sender as! UIView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.vaii.setImage(UIImage(named: "Resultados"), for: .normal)
            }, completion: nil)
            self.butao.isHidden = true
            self.novamente.isHidden = true

            
        case 7:
            UIView.transition(with: sender as! UIView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.vaii.setImage(UIImage(named: "FeedbackFinal"), for: .normal)
            }, completion: nil)
            
            vaii.isHidden = false
            self.butao.isHidden = true
            self.novamente.isHidden = false

            
            
        default:
            print("Ops")
        }
        
        
        
        
        numero = numero + 1
    }

        
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 215, height: 30))
        label.text = "Treino"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
        
        
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(title: "Ajuda",style: UIBarButtonItemStyle.plain,target: self, action: #selector(TreinoController.Ajuda) )
        self.navigationItem.setRightBarButtonItems([rightButton], animated: true)
        
        let defaults = UserDefaults.standard
        if !defaults.bool(forKey: "haveRanOnce") {
            DispatchQueue.main.async {
                let view: NavigController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AjudarTreino") as! NavigController
                self.present(view, animated: true, completion: nil)
            }
            
            // Update defaults
            defaults.set(true, forKey: "haveRanOnce")
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func Ajuda(){
        DispatchQueue.main.async {
            let view: NavigController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AjudarTreino") as! NavigController
            self.present(view, animated: true, completion: nil)
        }
        
    
        
        
    print("Ajuda function")
    }



}
