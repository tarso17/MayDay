//
//  TreinoViewController.swift
//  primeirosSocorros
//
//  Created by Saulo de Tarco Neves Oliveira on 16/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit
import WatchConnectivity

class TreinoViewController: UIViewController {

    @IBOutlet weak var dor7: UIButton!
    @IBOutlet weak var dor6: UIButton!
    @IBOutlet weak var dor5: UIButton!
    @IBOutlet weak var dor4: UIButton!
    @IBOutlet weak var dor3: UIButton!
    
    @IBOutlet weak var dor2: UIButton!
    @IBOutlet weak var dor1: UIButton!
    
    var session:WCSession?{
        
        didSet{
            if let session = session{
                session.delegate = self
                session.activate()
                
            }
        }
    }
    
    
    func click(){
        
        dor1.setBackgroundImage(UIImage(named:"Choque"), for: .normal)
        dor2.setBackgroundImage(UIImage(named:"Respiracao"), for: .normal)
        dor3.setBackgroundImage(UIImage(named:"Consciencia"), for: .normal)
        dor4.setBackgroundImage(UIImage(named:"Convulsao"), for: .normal)
        dor5.setBackgroundImage(UIImage(named:"PeleFria"), for: .normal)
        dor6.setBackgroundImage(UIImage(named:"PelePegajosa"), for: .normal)
        dor7.setBackgroundImage(UIImage(named:"SemPulso"), for: .normal)
    
    
    }
    
    @IBAction func dor1(_ sender: AnyObject) {
        if WCSession.isSupported(){
            
       
            dor1.setBackgroundImage(UIImage(named:"ChoqueSelecionado"), for: .normal)
            
            session?.sendMessage(["comando": "Estado de Choque"], replyHandler: { (response:[String : Any]) in
                
                
                
                
                
                
            }, errorHandler: { (Error) in
                print("errouuuu")
              

            })
            
        }
    }
    @IBAction func dor2(_ sender: AnyObject) {
        if WCSession.isSupported(){
          
            
            dor2.setBackgroundImage(UIImage(named:"RespiracaoSelecionado"), for: .normal)
            
            session?.sendMessage(["comando": "Parada Respiratória"], replyHandler: { (response:[String : Any]) in
                
                
                
                
                
                
            }, errorHandler: { (Error) in
                print("errouuuu")
            })
            
        }
        
    }
    @IBAction func dor3(_ sender: AnyObject) {
        if WCSession.isSupported(){
            
            
            dor3.setBackgroundImage(UIImage(named:"ConscienciaSelecionado"), for: .normal)
            
            session?.sendMessage(["comando": "Perda de Consciência"], replyHandler: { (response:[String : Any]) in
                
                
                
                
                
                
            }, errorHandler: { (Error) in
                print("errouuuu")
            })
        
           
            
        }
        
        
    }
    
 
    @IBAction func dor4(_ sender: Any) {
        if WCSession.isSupported(){
          
            
            dor4.setBackgroundImage(UIImage(named:"ConvulsaoSelecionado"), for: .normal)
            
            session?.sendMessage(["comando": "Convulsão"], replyHandler: { (response:[String : Any]) in
                
                
                
                
                
                
            }, errorHandler: { (Error) in
                print("errouuuu")
            })
            
            
            
        }

    }
    
    @IBAction func dor6(_ sender: Any) {
        if WCSession.isSupported(){
            
            dor6.setBackgroundImage(UIImage(named:"PelePegajosaSelecionado"), for: .normal)
            
            
            session?.sendMessage(["comando": "Pele pegajosa"], replyHandler: { (response:[String : Any]) in
                
                
                
                
                
                
            }, errorHandler: { (Error) in
                print("errouuuu")
            })
            
            
            
        }

    }
    
    @IBAction func dor5(_ sender: Any) {
        if WCSession.isSupported(){
            
            dor5.setBackgroundImage(UIImage(named:"PeleFriaSelecionado"), for: .normal)
            
            
            session?.sendMessage(["comando": "Pele Fria"], replyHandler: { (response:[String : Any]) in
                
                
                
                
                
                
            }, errorHandler: { (Error) in
                print("errouuuu")
            })
            
            
            
        }

    }
    
    @IBAction func dor7(_ sender: Any) {
        if WCSession.isSupported(){
            dor7.setBackgroundImage(UIImage(named:"SemPulsoSelecionado"), for: .normal)
            
         
            
            session?.sendMessage(["comando": "Sem Pulso"], replyHandler: { (response:[String : Any]) in
                
                
                
                
                
                
            }, errorHandler: { (Error) in
                print("errouuuu")
            })
            
            
            
        }

    }
    
    func Ajuda(){
    
        
            DispatchQueue.main.async {
                let view: NavigController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AjudarCasos") as! NavigController
                self.present(view, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 215, height: 30))
        label.text = "Casos"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
        
        let leftButton: UIBarButtonItem = UIBarButtonItem(title: "Limpar",style: UIBarButtonItemStyle.plain,target: self, action: #selector(TreinoViewController.click) )
        self.navigationItem.setLeftBarButtonItems([leftButton], animated: true)
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(title: "Ajuda",style: UIBarButtonItemStyle.plain,target: self, action: #selector(TreinoController.Ajuda) )
        self.navigationItem.setRightBarButtonItems([rightButton], animated: true)

        
        session = WCSession.default()
        if (session?.isPaired )!{
            print("tudo ok")
            
        }
        else {
            print("conecte-se ao apple watch" )
            
        }
        
        let defaultss = UserDefaults.standard
        if !defaultss.bool(forKey: "haveRanOnce") {
                DispatchQueue.main.async {
                let view: NavigController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AjudarCasos") as! NavigController
                self.present(view, animated: true, completion: nil)
            }

            // Update defaults
            defaultss.set(true, forKey: "haveRanOnce")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension TreinoViewController:WCSessionDelegate{
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("ativo ios")
    }
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
}
