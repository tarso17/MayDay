//
//  CasosController.swift
//  primeirosSocorros
//
//  Created by Carlos  Machado on 24/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class CasosController: UIViewController{
var conteudo = ["Urinar sobre queimadura de água-viva não ajuda na cicatrização.",
                "Chupar veneno de cobra após uma picada não tira parte do veneno da corrente sanguínea.",
                "Tomar uma dose de whisky não alivia a dor de dente. O efeito analgésico está relacionado ao gelo e não ao álcool.",
                "Passar pasta de dente sobre queimaduras não neutraliza a dor.",
                "Colocar bife cru no olho roxo não alivia a dor do machucado. Aposte sempre na boa e velha bolsa de gelo.",
                "Água oxigenada em cortes e feridas não tem efeito contra os germes. O ideal é limpar os machucados com água e sabão.",
                "Extrair ferrão de abelha com os dedos não impede de espalhar o veneno na corrente sanguínea.",
                "Evite posicionar a cabeça para trás para evitar ou conter sangramentos nasais. O ideal é deixar o sangue sair da cavidade, apertando apenas a cartilagem.",
                "Pacientes com ataque cardíaco não devem ficar deitados. O ideal é sentar e flexionar levemente os joelhos apoiando a cabeça.",
                "Não é indicado que uma pessoa sentindo fraqueza coloque sua cabeça entre as pernas. O ideal é que ela se deite e levante as pernas para cima, aumentando o fluxo sanguíneo no cérebro.",
                "Meio de fortuna para maca: Dois canos ou varas compridas e roupas com mangas.",
                "Meio de fortuna para maca: Uma porta.",
                "Meio de fortuna para maca: Dois canos ou varas compridas e cipó ou corda entrelaçados formando uma rede",
                "Meio de fortuna para maca: Dois canos ou varas compridas e um cobertor ou pano grande amarrados.",
                "Meio de fortuna para fratura: Revista ou papelão que imobilize o membro.",
                "Meio de fortuna para fratura no dedo: Palito de picolé e esparadrapo imobilizando a fratura.",
                "Meio de fortuna para luvas de látex: Saco plástico.",
                "Nunca retire o capacete de uma vítima em um acidente. Isso pode machucar ou piorar uma fratura de cervical, por exemplo."]
     let rand = Int(arc4random_uniform(10))
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 250, height: 30))
        label.text = "Casos"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
//        let A: UInt32 = 10 // UInt32 = 32-bit positive integers (unsigned)
//        let B: UInt32 = 20
//        let number = arc4random_uniform(B - A + 1) + A
//        print(number)
        let rand = Int(arc4random_uniform(10))
        
        print(rand)
        print(conteudo[rand])
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func NotificationButton(_ sender: Any) {
        let content = UNMutableNotificationContent()
        content.title = "Curiosidade"
        content.subtitle = ""
        content.body = String(describing: conteudo[rand])
        content.badge = 1
        
        //TRIGGER
        
        var dataComponents = DateComponents()
        dataComponents.hour = 14
        dataComponents.minute = 00
        
        let trigger =  UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        //        let trigger =  UNCalendarNotificationTrigger(dateMatching: dataComponents, repeats: false)
        let requestIdentifier = "Queez"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: {error in
            // handle error
        })
        

        
        
    }

}
