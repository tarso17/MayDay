//
//  SobreController.swift
//  primeirosSocorros
//
//  Created by Carlos  Machado on 24/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import Foundation
import UIKit

class SobreController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 250, height: 30))
        label.text = "Sobre"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
