//
//  KitController.swift
//  primeirosSocorros
//
//  Created by Thiago Bessa on 30/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit

class KitController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 250, height: 30))
        label.text = "KIT"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        self.navigationItem.titleView = label
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
