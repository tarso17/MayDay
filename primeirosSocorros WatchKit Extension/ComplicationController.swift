//
//  ComplicationController.swift
//  primeirosSocorros WatchKit Extension
//
//  Created by Thiago Bessa on 07/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import ClockKit
import WatchKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    var texto = "MayDay"
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.forward, .backward])
    }
    
    func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.showOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        // Call the handler with the current timeline entry
      
         //let beerGlass = UIImage(named: "Coracao")
        
        switch complication.family {
        case .modularLarge:
            let template = CLKComplicationTemplateModularLargeStandardBody()
            template.headerTextProvider = CLKSimpleTextProvider(text: texto)
         
            template.body1TextProvider = CLKSimpleTextProvider(text:"Entrar no App")
            //template.headerImageProvider = CLKImageProvider(onePieceImage: (UIImage(named:"Coracao"))!)
           // template.body2TextProvider = CLKSimpleTextProvider(text: "Aplicaticas de P.S")
            let entry = CLKComplicationTimelineEntry(date: NSDate() as Date, complicationTemplate: template)
           ///////
            handler(entry)

            
     
        default:
            
            handler(nil)
        }
        
    }
    
    
    func getTimelineEntries(for complication: CLKComplication, before date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntries(for complication: CLKComplication, after date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Placeholder Templates
    
    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        // This method will be called once per supported complication, and the results will be cached
        handler(nil)
    }
    func getPlaceholderTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        switch complication.family {
        case .modularLarge:
            let template = CLKComplicationTemplateModularLargeStandardBody()
            template.headerTextProvider = CLKSimpleTextProvider(text: texto)
            
            template.body1TextProvider = CLKSimpleTextProvider(text:"Entrar no App")
            //template.headerImageProvider = CLKImageProvider(onePieceImage: (UIImage(named:"Coracao"))!)
            // template.body2TextProvider = CLKSimpleTextProvider(text: "Aplicaticas de P.S")
            // let entry = CLKComplicationTimelineEntry(date: NSDate() as Date, complicationTemplate: template)
            ///////
            handler(template)
        default:
            print("ops")
        }
       

    
}
}
