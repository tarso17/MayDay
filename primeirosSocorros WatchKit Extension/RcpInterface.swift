//
//  RcpInterface.swift
//  primeirosSocorros
//
//  Created by Saulo de Tarco Neves Oliveira on 16/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit

import WatchKit
import CoreMotion
//falta trabalhar com movimentos

class RcpInterface: WKInterfaceController {
    var timer = Timer()
    var rodando = Bool()
    var x:Int = 1

    @IBOutlet private weak var heart: WKInterfaceImage!
    @IBOutlet var Iniciar_out: WKInterfaceButton!
    func scheduledTimerWithTimeInterval(){
      
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    override func willDisappear() {
                timer.invalidate()
    }
    
    
    
    func updateCounting(){
        rodando = true
        //NSLog("counting..")
        print(x)
        Iniciar_out.setBackgroundImageNamed("BotaoParar")

        WKInterfaceDevice.current().play(.directionUp)
        self.animateHeart()
        self.animate(withDuration: 0.1, animations: {
            self.heart.setWidth(70)
            self.heart.setHeight(70)
            self.x = self.x+1
        })
    
    }

    @IBAction func iniciar_action() {
        if rodando == false{
            timer.invalidate()
            scheduledTimerWithTimeInterval()
            
           
        }
        else {
           
            timer.invalidate()
            Iniciar_out.setBackgroundImageNamed("BotaoIniciar")
            rodando = false
           
        }
        

    }
    
    func animateHeart() {
        self.animate(withDuration: 1) {
            self.heart.setWidth(60)
            self.heart.setHeight(60)
        }

}
}
