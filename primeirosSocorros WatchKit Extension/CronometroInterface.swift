//
//  CronometroInterface.swift
//  primeirosSocorros
//
//  Created by Saulo de Tarco Neves Oliveira on 16/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit
import WatchKit
import UserNotifications

class CronometroInterface: WKInterfaceController {
    
    
    @IBOutlet var cronometroText: WKInterfaceLabel!
    
    @IBOutlet var acaobu: WKInterfaceButton!
    var tempo = Timer()
    var totalTempo =  TimeInterval()
    var lastInterval = TimeInterval()
    var variavelx: String!
    var noti = true
    override func willDisappear() {
        
   
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
       
    }
    
    override func willActivate() {
        super.willActivate()
       
    }
    
    override func didDeactivate() {
        super.didDeactivate()
       //tempo.invalidate()
    }

    @IBAction func ShowIniciar() {
        WKInterfaceDevice.current().play(.click)

        if !tempo.isValid{
        let timerSelector: Selector = #selector(CronometroInterface.timerUpdade)
            acaobu.setBackgroundImageNamed("BotaoPausar")

        tempo = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: timerSelector, userInfo: nil, repeats: true)
        lastInterval = NSDate.timeIntervalSinceReferenceDate
            
        }else{
                tempo.invalidate()
            cronometroText.setText("Continuar")
            acaobu.setBackgroundImageNamed("BotaoIniciarCronometro")
        }
    }
    
    
    
    func timerUpdade(){
        let elements = getTimerElements()
        let min = elements.minutos
        let sec = elements.segund
        let milli = elements.milli
        cronometroText.setText("\(min):\(sec):\(milli)")
        
        variavelx = sec
        
        switch variavelx {
        case "05":
            presentAlert(withTitle: "Fique observando", message:" já passou 5 seg", preferredStyle: WKAlertControllerStyle.alert, actions: [.init(title: "Voltar", style: WKAlertActionStyle.default, handler: {})])
        case "15":
            presentAlert(withTitle: "Fique observando", message:" já passou 15 seg", preferredStyle: WKAlertControllerStyle.alert, actions: [.init(title: "Voltar", style: WKAlertActionStyle.default, handler: {})])
        default: break
            
        }
        
    }
    
    func getTimerElements() -> (minutos: String, segund: String, milli: String){
        let now = NSDate.timeIntervalSinceReferenceDate
        
        totalTempo = totalTempo + (now - lastInterval)
        
        lastInterval = now
        
        var displayTime = totalTempo
        //minutos
        let min = Int(displayTime/60)
        displayTime = displayTime - (TimeInterval(min) * 60)
        let minStr = String(format: "%02d", min)
        
        //segundo
        let seg = Int(displayTime)
        displayTime = displayTime - (TimeInterval(seg))
        let segStr = String(format: "%02d", seg)
        
        //minutos
        let milli = Int(displayTime*100)
        let milliStr = String(format: "%02d", milli)
        
        return(minStr, segStr, milliStr)
    }
    
    @IBAction func ShowZerar() {
        WKInterfaceDevice.current().play(.click)
        tempo.invalidate()
        cronometroText.setText("00:00:00")
        acaobu.setBackgroundImageNamed("BotaoIniciarCronometro")
        totalTempo = TimeInterval()
        noti = true
    }
    
    
   

    
}

