//
//  CasosInterface.swift
//  primeirosSocorros
//
//  Created by Saulo de Tarco Neves Oliveira on 16/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import UIKit
import WatchKit
import  WatchConnectivity

class CasosInterface: WKInterfaceController {
    var session:WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activate()
            }
        }
    }
    
    @IBOutlet var labellll: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        
        
        
        
        
        
        
    }
    override func didAppear() {
        session = WCSession.default()
        //        session?.sendMessage(["comando": "pedirQRcode"], replyHandler: { (response:[String : Any]) in
        //
        //
        //
        //
        //
        //            }, errorHandler: { (Error) in
        //                print("errouuuu")
        //        })
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
       
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    

    

}

extension CasosInterface:WCSessionDelegate{
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("ativo watch")
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        let text = message["comando"] as! String
        print(text)
        WKInterfaceDevice.current().play(.directionUp)
        print("recebeu ")
        presentAlert(withTitle: "", message:text, preferredStyle: WKAlertControllerStyle.alert, actions: [.init(title: "Confirmar", style: WKAlertActionStyle.default, handler: {})])
        
        //labellll.setText(text)
    }
    
}



