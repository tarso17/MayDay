//
//  InterfaceController.swift
//  primeirosSocorros WatchKit Extension
//
//  Created by Thiago Bessa on 07/11/16.
//  Copyright © 2016 Thiago Bessa. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func ShowLigar() {
        
            if let telURL=NSURL(string:"tel:192") {
                let wkExtension=WKExtension.shared()
                wkExtension.openSystemURL(telURL as URL)
                wkExtension.didChangeValue(forKey: "show1")
                
            }
    }
}
